@isTest
public class BypassVRTest {
    
    @isTest
    private static void toggleBypassContactVR(){
        // create a contact
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        Contact con = new Contact(FirstName = 'john', Lastname = 'Cena', AccountId = acc.Id);
        
        Exception throwedException;
        try{            
            insert con;    
        }catch(Exception e){
            throwedException = e;
        }
        system.assert(throwedException != null && throwedException.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') , 'Sans bypass, la règle de validation doit être levée');
        
        Exception throwedException2;
        try{    
            con.TECHToggleBypassVR__c = true;
            insert con;    
        }catch(Exception e){
            throwedException2 = e;
        }
        
        system.assertEquals(null, throwedException2, 'Avec bypass, la règle de validation ne doit pas être levée.');
        
        
        Exception throwedException3;
        try{    
            update con;    
        }catch(Exception e){
            throwedException3 = e;
        }
        
        system.assert(throwedException3 != null && throwedException3.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') , 'Sans bypass, la règle de validation doit être levée');
        
        
        Exception throwedException4;
        try{    
            con.TECHToggleBypassVR__c = !con.TECHToggleBypassVR__c; //toggle the current value to bypass vr on update
            update con;    
        }catch(Exception e){
            throwedException4 = e;
        }
        
        system.assertEquals(null, throwedException4, 'Avec bypass, la règle de validation ne doit pas être levée.');
        
    }
    
    @isTest
    private static void regularBypassContactVR(){
        // create a contact
        Account acc = new Account(Name = 'TestAccount');
        insert acc;
        Contact con = new Contact(FirstName = 'john', Lastname = 'Cena', AccountId = acc.Id);
        
        Test.startTest();
        Exception throwedException;
        try{            
            insert con;    
        }catch(Exception e){
            throwedException = e;
        }
        
        system.assert(throwedException != null && throwedException.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') , 'Sans bypass, la règle de validation doit être levée');
        
        Exception throwedException2;
        try{    
            con.TECHRegularBypassVR__c = true;
            insert con;    
        }catch(Exception e){
            throwedException2 = e;
        }
        
        system.assertEquals(null, throwedException2, 'Avec bypass, la règle de validation ne doit pas être levée.');
        con = [Select TECHRegularBypassVR__c from Contact limit 1];
        system.assertEquals(False, con.TECHRegularBypassVR__c, 'le checkbox doit être resetté à false');
        
        Exception throwedException3;
        try{    
            update con;    
        }catch(Exception e){
            throwedException3 = e;
        }
        
        system.assert(throwedException3 != null && throwedException3.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION') , 'Sans bypass, la règle de validation doit être levée');
        
        
        Exception throwedException4;
        try{    
            con.TECHRegularBypassVR__c = true; 
            update con;    
        }catch(Exception e){
            throwedException4 = e;
        }
 		Test.stopTest();
        
        system.assertEquals(null, throwedException4, 'Avec bypass, la règle de validation ne doit pas être levée.');
        con = [Select TECHRegularBypassVR__c from Contact limit 1];
        system.assertEquals(False, con.TECHRegularBypassVR__c, 'le checkbox doit être resetté à false');
    }
    
}