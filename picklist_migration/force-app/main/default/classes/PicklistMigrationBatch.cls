/**
* @author         VNJ
* @Description    Picklist values migration
* @examples: replace on task records Type values 'Call' by 'Meeting'
String objectName = 'Task';
String whereClause = '';
String fieldName = 'Type';
Map<String, String> sourceTargetValues = new Map<String, String>{ 'Call' => 'Meeting'};
Database.executeBatch(new PIcklistMigrationBatch(objectName, whereClause, fieldName,sourceTargetValues), 200);
*/
global with sharing class PicklistMigrationBatch implements Database.Batchable<sObject> {
    private String objectName;
    private String whereClause;
    private String picklistFieldName;
    private Map<String,String> sourceTargetValues;
    

    global PicklistMigrationBatch(String objectName, String whereClause, String picklistFieldName, Map<String,String> sourceTargetValues) {
        this.objectName = objectName;
        this.whereClause = whereClause;
        this.picklistFieldName = picklistFieldName;
        this.sourceTargetValues = sourceTargetValues;
    }

    global Database.Querylocator start(Database.BatchableContext BC){
        List<String> sourceValues = new List<String>(sourceTargetValues.keySet());
        String queryWhere = String.isNotBlank(whereClause) ? ' AND '+whereClause : '';
        String query = 'Select Id, ' + this.picklistFieldName +
            ' FROM ' + this.objectName + 
            ' WHERE ' + this.picklistFieldName + ' IN:sourceValues' +queryWhere;
            
        system.debug('records: ' + Database.getQueryLocator(query));
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.Batchablecontext BC, List<sObject> scope){
        system.debug('*** PicklistMigrationBatch Execute Method Begins. scope: ' + scope);
        List<sObject> targetRecords = new List<sObject>();
        for(sObject record: scope){
            for(String sourceValue : sourceTargetValues.KeySet()){
                if((String)record.get(this.picklistFieldName) == sourceValue){
                    record.put(this.picklistFieldName, sourceTargetValues.get(sourceValue));
                }
            }
            targetRecords.add(record);
        }
        update targetRecords;
        system.debug('*** PicklistMigrationBatch Execute Method Ends');
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('*** PicklistMigrationBatch Finish Method Ends');
    }
}