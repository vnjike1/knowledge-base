@isTest
public with sharing class PicklistMigrationBatchTest {
    public static final INTEGER NB_TASKS_CREATED = 100;
    public static final STRING TASK_TYPE_SOURCEVALUE = 'Call';
    public static final STRING TASK_TYPE_TARGETVALUE = 'Meeting';
    
    @testSetup public static void setup(){
        List<Task> tasks = new List<Task>();
        for(Integer i=0; i<NB_TASKS_CREATED; i++) {
            Task t = new Task();
            t.subject = 'test subject';
            t.ActivityDate = System.today();
            if(Math.mod(i, 2) == 0){ //si nombre pair
                t.Status = 'Open';
            }else{
                t.Status = 'Closed';
            }
            t.Priority = 'Normal';
            t.Type = TASK_TYPE_SOURCEVALUE;
            
            tasks.add(t);
        }
        insert tasks;
    }
    
    
    @isTest public static void picklistMigrationTest(){
        
        Map<String, String> sourceTargetValues = new Map<String, String>{ TASK_TYPE_SOURCEVALUE => TASK_TYPE_TARGETVALUE};
            
            //Check that new value is not set
        List<Task> tasks = [Select Id, Type from Task];
        for(task t: tasks) {
            system.assertNotEquals(TASK_TYPE_TARGETVALUE, t.Type);
        }
        System.Test.startTest();
        Database.executeBatch(new PIcklistMigrationBatch('Task', null, 'Type',sourceTargetValues), 200);
        System.Test.stopTest();
        
        //Check that new value is set
        tasks = [Select Id, Type from Task];
        for(task t: tasks) {
            system.assertEquals(TASK_TYPE_TARGETVALUE, t.Type);
        }
            
        
    }

    @isTest public static void picklistMigrationWithWhereClauseTest(){
        
        Map<String, String> sourceTargetValues = new Map<String, String>{ TASK_TYPE_SOURCEVALUE => TASK_TYPE_TARGETVALUE};
            
            //Check that new value is not set
        List<Task> tasks = [Select Id, Type from Task];
        for(task t: tasks) {
            system.assertNotEquals(TASK_TYPE_TARGETVALUE, t.Type);
        }
        System.Test.startTest();
        Database.executeBatch(new PIcklistMigrationBatch('Task', 'Status = \'Open\'', 'Type',sourceTargetValues), 200);
        System.Test.stopTest();
        
        //Check that new value is set only on Open Task
        system.assertEquals(NB_TASKS_CREATED/2,  [Select Id, Type from Task where Type = :TASK_TYPE_TARGETVALUE].size());
        
    }
    
}