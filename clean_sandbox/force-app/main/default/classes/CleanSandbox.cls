/* exemple éxécution  
 String queriesText = 'select Id from Opportunity,select Id from Contract';
 Database.executeBatch(new CleanSandbox(queriesText)); 
*/
global class CleanSandbox implements Database.Batchable<sObject> {
    /*Define the scope of records to delete*/
    private string queriesText = '';
    
    global CleanSandbox(String queriesText){
        this.queriesText = queriesText;
    }
    
    global Iterable<sObject> start(Database.BatchableContext BC){
        return new CustomIterable(queriesText);
    }
        
    global void execute(Database.Batchablecontext BC, List<sObject> scope){
        system.debug('*** CleanSandbox Execute Method Begins');
        system.debug('scope: '+ scope);
        sObject[] recordsToDelete = scope;
        Database.delete(recordsToDelete, false); //partial delete
    }
    
    global void finish(Database.BatchableContext BC){
        system.debug('*** CleanSandbox Finish Method Ends');
    }

    global class CustomIterator implements Iterator<SObject>
    {
        private List<SObject> records;
        private Integer currentIndex;
        
        global CustomIterator(List<SObject> records)
        {
            this.records = records;
            currentIndex =0;
        }
        
        global boolean hasNext(){
            if( currentIndex  >= records.size()) {
                return false;
            }else{
                return true;
            }
        }
        
        global sObject next(){
            if(currentIndex  == records.size()) { return null;}
            currentIndex ++;
            return records[currentIndex -1];
            
        }
        
    }
    
    global class CustomIterable implements Iterable<SObject>
    {	
        private String queriesText;

        CustomIterable(String queriesText){
            this.queriesText = queriesText;
        }
        
        global Iterator<sObject> iterator(){
            List<sObject> records = new List<sObject>();
            String[] queries = queriesText.split(',');
            for(String query: queries){
                records.addAll(Database.query(query.trim()));
            }
            return new CustomIterator(records);
        }
    }
    
}