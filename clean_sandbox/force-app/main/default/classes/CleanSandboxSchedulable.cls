/**
* @Description 	Schedulable Class for CleanSandbox
 system.schedule('delete old records every day at 23h30',  '0 30 23 * * ?', new CleanSandboxSchedulable());
*/   
global class CleanSandboxSchedulable implements Schedulable{
	global void execute(SchedulableContext sc){
        Integer batchSize = 200;      
        String queriesText = 'select Id from Contact,select Id from Opportunity';
        database.executebatch(new CleanSandbox(queriesText), batchSize);
    }
}