@isTest
public class CleanSandboxTest {
    public static final Integer RECORDS_NUMBER = 5;
    
    @testsetup
    private static void setup(){
        Account acc = new Account (Name = 'testAccount');
        insert acc;
        Contact[] cons = new Contact[]{};
        for(Integer index=0; index<RECORDS_NUMBER ; index++){
            cons.add(new Contact(AccountId = acc.Id, lastName = 'l'+index, firstName = 'f'+index));
        }
        insert cons;
        
        Opportunity[] opps = new Opportunity[]{};
        for(Integer index=0; index<RECORDS_NUMBER ; index++){
            opps.add(new Opportunity(AccountId = acc.Id, Name = 'testOpp'+index, CloseDate = Date.today(), stageName = 'Brouillon'));
        }
        insert opps;
        
    }
    
    @isTest
    private static void deleteRecordsTest(){
        Contact[] cons = [Select Id from Contact];
        Opportunity[] opps = [Select Id from Opportunity];
        system.assertEquals(RECORDS_NUMBER, cons.size());
        system.assertEquals(RECORDS_NUMBER, opps.size());
        
        Test.startTest();
        String queriesText = 'select Id from Contact,select Id from Opportunity';
        Database.executeBatch(new CleanSandbox(queriesText));
        Test.stopTest();
        
        cons = [Select Id from Contact];
        opps = [Select Id from Opportunity];
        system.assert(cons.isEmpty(), 'Tous les records cons doivent être supprimés après éxécution du batch. Actual: ' + cons.size());
        system.assert(opps.isEmpty(), 'Tous les records opps doivent être supprimés après éxécution du batch. Actual: ' + opps.size());
        
    }
    
    
    @isTest 
    private static void cleanSandboxSchedulableTest(){
        test.startTest();
        system.schedule('Test delete old records every day at 23h30',  '0 30 23 * * ?', new CleanSandboxSchedulable());
        test.stopTest();
        
    }
    
}